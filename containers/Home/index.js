import React from 'react';
//import {Link} from 'react-router-dom';
import Header from '../../components/Header';
import SideMenu from '../../components/SideMenu';
import Content from '../../components/Content';
import './index.css'
const Home = () => {
    return (
        <div id="site-wrap" data-mode="0" data-progid="GdQl8awmSCiAJYITAD3Rlg" data-org="0FeVcxixS5byAJYJ1AEyt2g" data-ts="0" data-tp="16.9.2018">
            <Header />
            <SideMenu />
            <Content />
        </div>
    )
}

export default Home 