import React from 'react'


const SideMenuBigIcon = (props) => {
    return (
        <div id="progimg" className="program-box border-thin">
            <div className="popup-parent">
                <div id="progimg-img" className="program-image70m pointer" style={{backgroundImage: `url(${props.image})`}}></div>
            </div>
        </div>
    )
}

export default SideMenuBigIcon