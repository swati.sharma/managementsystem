import React from 'react'
import InputBox from '../InputBox';
import DropDown from '../DropDown';

const Header = () => {
    const Products = [
        {   
            id: "28D63F89DFE5A9B38025824B003F0241;0",
            name:'Product #1'
        },
        {   
            id: "75CE95BBF83F750C8025824B003F2DF0;0",
            name:'Product #2'
        },
        {   
            id: "F6B9FC207185B76980257FD2003EBECA;0",
            name:'Product #3'
        },
        {   
            id: "9580CBD1FFE909D98025824B003EBFB7;0",
            name:'Product #4'
        },
        {   
            id: "37A1EA60CBFB89D8802582500044905A;0",
            name:'Product #5'
        },
        {   
            id: "F6B9FC207185B76980257FD2003EBECE;0",
            name:' '
        }
    ]
    const Services = [{
        id: "A8A9346757467C2D80258274006378FF",
        name: "Administration"
    },{
        id: "94F314E374FEA9008025827400637998",
        name: "Agriculture"
    },{
        id: "35AE106DFCFE7C6C8025827400637FD9",
        name: " "
    }]

    const Information = [{
        id:"A2F12E4D233698BC802582750051A388",
        name: "Information"
    }]
    
    return (
        <div id="global-nav">
            <div id="nav-wrapper">
                    <div className="unit main-logo-wrap pointer">
                        <span className="main-logo"></span>
                    </div>

                    <div className="orglabel">
                        <span>Home</span>
                    </div>
                    <DropDown label="Products" options={Products} width="326px" dataId ="6BD4D0BFC073CF80802582740069C44D" minWidth="222px"/>
                    <DropDown label="Services" options={Services} width="400px" dataId ="6BD4D0BFC073CF80802582740069C44D" minWidth="180px"/>
                    <DropDown label="i" options={Information} width="150px" dataId ="31B8C95AF43AF823802582750051E745" minWidth="150px"/>
                    <InputBox id="scsearch" placeholder="Search..." className="ui-autocomplete-input" autoComplete="off"/>
                </div>
        </div>
    )
}

export default Header