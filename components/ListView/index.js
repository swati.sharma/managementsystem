import React from 'react'
;
const ListView = (props) => {
    const imagePath = "../../Assets/bigicon.png"

    return (
        <div id="proglist" className="program-list">
            <nav>
                <ul data-default="19D425F1AC26482880258213003DD196">
                        { 
                            props.menuArray.map((val, index) => {
                                return(
                                    <li className={val.id === props.selectedId ? "list-item selected" : "list-item"}  key={val.id}>
                                        <a className="list-item-button" style={{position:"relative"}} data-orgid="" data-orgn="" title="Report #1" data-ot={val.dataOt} data-t={val.dataT}>
                                        <div className="program-list-img-frame">
                                            <div className="program-list-img program-image70m" style={{backgroundImage:`url(${imagePath})`}}></div>
                                        </div>{val.name}</a>
                                    </li>
                                )
                            }) 
                        }
                </ul> 
            </nav>
        </div>
    )
}

export default ListView