import React from 'react'

const DropDown = (props) => {
    return (
        <div className="orglabel">
            <span>{props.label}</span>
            <div className="orgdrop" style={{width : props.width}} data-id={props.dataId}>
                <div className="orgdrop-col" style={{minWdth:props.minWdth}}>
                    <ul>
                        {
                            props.options && props.options.map(option =>{
                                return (
                                <li data-ida={option.id} className="" key={option.id}>
                                    <a>{option.name}</a>
                                </li>)
                            })
                        }
                    </ul>
                </div>
            </div>
        </div>
    )
}
export default DropDown