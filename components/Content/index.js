import React from 'react'
import Footer from '../Footer';

const Content = () => {
    return (
        <div>
            <div id="page-header"></div>
            <div id="sc-dimd"></div>
            <div id="xv-body">
                <div className="lenssc">
                    <div id="orgsel-wrap">
                        <div id="orgsel"></div>
                    </div>
                    <div id="scdiv" className="line scorecardUnit">
                        <div className="sc-container">
                                
                        </div>
                    </div>
                    <Footer />
                </div>
        </div>
        </div>
    )
}
export default Content