import React from 'react'

const InputBox = (props) => {
    return (
        <div className="sc-search-wrap">
            <input {...props}/>
        </div>
    )
}

export default InputBox