import React from 'react'
import SideMenuBigIcon from '../SldeMenuBigIcon';
import ListView from '../ListView';


const SideMenu = () => {

    const imagePath = "../../Assets/bigicon.png"


    const menuItem = [
        { 
            name :"Report #1",
            id: '19D425F1AC26482880258213003DD196',
            dataOt:"6;10;11;12;13;16" ,
            dataT:"0"
        },
        { 
            name :"Report #2",
            id: '4D8026226060BDB780258322004F6415',
            dataOt:"16" ,
            dataT:"0"
        },
        { 
            name :"Report #3",
            id: 'BEBBEAE237342D0780258431006E87F7',
            dataOt:"20" ,
            dataT:"1"
        },
        { 
            name :"Report #4",
            id: 'CEAAA5EC1B2484A9802582DC006E3F67',
            dataOt:"11" ,
            dataT:"1"
        },
        { 
            name :"Report #5",
            id: '220FBB9A66756A59802582B900577392',
            dataOt:"11" ,
            dataT:"1"
        },
        { 
            name :"Report #6",
            id: '3EB2B7E9DF134BF48025827500651EA8',
            dataOt:"6;1;10;11;12;13;17;9;15;16" ,
            dataT:"1"
        },
        { 
            name :"Report #7",
            id: 'C0C779809DB2C3EA80258275006CB0C7',
            dataOt:"6;1;10;11;12;13;17;9;15;16" ,
            dataT:"1"
        },

    ]
    return (
        <div className="programs">
            <SideMenuBigIcon image={imagePath}/>
            <ListView menuArray={menuItem} selectedId= "19D425F1AC26482880258213003DD196"/>
        </div>
    )
}

export default SideMenu